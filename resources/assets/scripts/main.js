/** import external dependencies */
import $ from 'jquery/dist/jquery.js';
import "waypoints/lib/jquery.waypoints.min";
import 'jquery-validation/dist/jquery.validate';

/** import local dependencies */
import Router from './util/Router';
import './util/_utils';
import './util/browser';
import common from './routes/common';
import pageTemplateTemplateHome from './routes/home';
import pageTemplateTemplateAbout from './routes/about';
import pageTemplateTemplateProducts from './routes/products';
import pageTemplateTemplateContact from './routes/contact';
import pageTemplateTemplateWhereToBuy from './routes/where-to-buy';

import pageTemplateTemplateHomeDemo from './routes/home-demo';

const home = pageTemplateTemplateHome;

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
    /** All pages */
    common,
    /** Home page */
    home,
    pageTemplateTemplateHome,
    pageTemplateTemplateAbout,
    pageTemplateTemplateProducts,
    pageTemplateTemplateContact,
    pageTemplateTemplateWhereToBuy,

    pageTemplateTemplateHomeDemo,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
