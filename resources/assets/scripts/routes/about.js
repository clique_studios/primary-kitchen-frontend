import Swiper from 'swiper/dist/js/swiper';
import TweenMax from "gsap/src/minified/TweenMax.min";
import "jquery-accessible-modal-window-aria";

export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        if (!inIframe()) {
            //Quote Ticker
            if ($(".quote-ticker-item").length > 0) {
                var duration = $(window).width() > 581 ? 1 : 2;
                var quoteSection = $("#quote-ticker");
                var quote_ticker = TweenMax.to(quoteSection.find(".quote-ticker-inner"), 30, {
                    x: "-100%",
                    ease: Linear.easeNone,
                    repeat: -1
                });
                quote_ticker.pause();
                quote_ticker.timeScale(duration);
                var tickerWaypoint = new Waypoint({
                    element: quoteSection[0],
                    handler: function (direction) {
                        if (direction === "down") {
                            quote_ticker.play();
                        } else {
                            quote_ticker.pause();
                        }
                    },
                    offset: "70%",
                });

                $(window).on("resize", function () {
                    if($(window).width() > 581) {
                        quote_ticker.timeScale(1);
                    } else {
                        quote_ticker.timeScale(2);
                    }
                });
                quoteSection.hover(function () {
                    quote_ticker.timeScale(0.25);
                }, function () {
                    if($(window).width() > 581) {
                        quote_ticker.timeScale(1);
                    } else {
                        quote_ticker.timeScale(2);
                    }
                });
            }

            //Our Team
            var slider = $(".team-inner");
            var sectionEnd = $(".team-col:last-child").position().left - $(".team-col:last-child").width() - parseFloat($(".team-inner").css("padding-left")) + ($(window).width() * .1);
            var tl = TweenMax.to(slider[0], 5, {ease: Linear.easeNone, x: (sectionEnd * -1), rotation: 0.01});
            tl.pause();
            var right_hover = $(window).width() / 5;
            var left_hover = $(window).width() - right_hover;

            $(window).on("resize", function () {
                right_hover = $(window).width() / 4;
                left_hover = $(window).width() - right_hover;
                sectionEnd = $(".team-col:last-child").position().left - $(".team-col:last-child").width() - parseFloat($(".team-inner").css("padding-left")) + ($(window).width() * .1);
                tl.updateTo({x: (sectionEnd * -1)});
            });

            $("#our-team").on("mousemove", function (e) {
                if ($(window).width() > 1024) {
                    if (e.clientX > left_hover && (tl.paused() || tl.reversed())) {
                        tl.play();
                    }
                    if (e.clientX < right_hover && (tl.paused() || !tl.reversed())) {
                        tl.reverse();
                    }
                    if (e.clientX < left_hover && e.clientX > right_hover && !tl.paused()) {
                        tl.pause();
                    }
                }
            });

            //Our Process
            if ($("#our-process .swiper-slide").length > 0) {
                var processImageSwiper = new Swiper("#our-process .swiper-container.image-swiper", {
                    slidesPerView: "auto",
                    speed: 1000,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    a11y: {
                        prevSlideMessage: 'Previous slide',
                        nextSlideMessage: 'Next slide',
                    },
                    simulateTouch: false,
                });

                var processCaptionSwiper = new Swiper("#our-process .swiper-container.caption-swiper", {
                    slidesPerView: "auto",
                    speed: 1000,
                    effect: 'fade',
                    a11y: {
                        prevSlideMessage: 'Previous slide',
                        nextSlideMessage: 'Next slide',
                    },
                    simulateTouch: false,
                });

                processImageSwiper.on('transitionStart', function () {
                    var active_index = processImageSwiper.activeIndex;
                    if(processImageSwiper.isEnd){
                        active_index++;
                    }
                    processCaptionSwiper.slideTo(active_index);
                })
            }
        }
    },
};
