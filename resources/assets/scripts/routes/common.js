import Swiper from 'swiper/dist/js/swiper';
import 'selectric/src/jquery.selectric';
import TweenMax from "gsap/src/minified/TweenMax.min";

export default {
    init() {
        //Header Menu
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        function menuSwiper() {
            if ($(window).width() <= 581 && menu.hasClass("swiper-view")) {
                menu.removeClass("swiper-view");
                if (menuSlider !== undefined) {
                    menuSlider.destroy();
                }
            } else if ($(window).width() > 581 && !menu.hasClass("swiper-view")) {
                menu.addClass("swiper-view");
                menuSlider = new Swiper(menu.find(".swiper-container")[0], {
                    centeredSlides: true,
                    speed: 1000,
                    a11y: {
                        prevSlideMessage: 'Previous Menu Link',
                        nextSlideMessage: 'Next Menu Link',
                    },
                    mousewheel: true,
                    scrollbar: {
                        el: '.swiper-scrollbar',
                        draggable: true,
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    simulateTouch: false,
                });
                menu.find(".nav-item").each(function (i) {
                    var href = $(this).attr("href");
                    var current_url = window.location.pathname;
                    if (href === current_url) {
                        menuSlider.slideTo(i);
                    }
                })
            }
        }

        if (!inIframe()) {
            var menuSlider;
            var menu = $("#menu");
            var header = $("header.header");

            $(window).ready(function () {
                $("[data-iframe-src]").each(function () {
                    $(this).attr("src", $(this).attr("data-iframe-src"));
                });
                menuSwiper(menuSlider, menu);
            });

            $(window).on("resize", function () {
                menuSwiper(menuSlider, menu);
            });

            $("#hamburger").on("click", function () {
                menu = $(this).parent().parent().next();
                header = menu.parent();
                if (!menu.hasClass("live-page-menu")) {
                    menu.addClass("live-page-menu");
                }
                menu.toggleClass("show");
                header.toggleClass("show");
                $("body").toggleClass("no-scroll");
                $(this).toggleClass("open-menu");
                menu.css("transform", "translateX(0)");
                if (menu.hasClass("show")) {
                    menu.find(".nav-item, .swiper-button-prev, .swiper-button-next").attr("tabIndex", 0);
                    header.css("overflow", "visible");
                    $('html,body').animate({scrollTop: 0}, 'slow');
                    $("#menu .nav-item, #menu h2").each(function () {
                        var overlay = $(this);
                        overlay.addClass("delay");
                        setTimeout(function () {
                            overlay.removeClass("delay");
                        }, 1250);
                    });
                    setTimeout(function () {
                        menu.css("opacity", "1");
                        menu.find("iframe, .overlay").addClass("delay");
                        setTimeout(function () {
                            menu.find("iframe, .overlay").removeClass("delay");
                        })
                    }, 250);
                } else {
                    menu.find(".nav-item, .swiper-button-prev, .swiper-button-next").attr("tabIndex", -1);
                    if ($(window).width() > 581) {
                        setTimeout(function () {
                            menu.css({
                                transform: "translateX(100vw)",
                                opacity: 1,
                            });
                            header.removeAttr("style");
                        }, 1250);
                    } else {
                        menu.css({
                            transform: "translateX(100vw)",
                            opacity: 1,
                        });
                        header.removeAttr("style");
                    }
                }
            });
        }

        $("#menu .nav-item").on("click", function (e) {
            if($(window).width() > 581) {
                e.preventDefault();
                var nav_item = $(this);
                nav_item.addClass("clicked");
                var href = $(this).attr("href");
                var current_url = window.location.pathname;
                $("#menu").removeClass("show");
                $("body").removeClass("no-scroll");
                $('#menu-container').animate({scrollTop: 0}, 1000);
                if (href !== current_url) {
                    window.history.pushState("object or string", "Title", href);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1250);
                } else {
                    setTimeout(function () {
                        header.removeClass("show");
                        $("#hamburger").removeClass("open-menu");
                        nav_item.removeClass("clicked");
                        $("#menu").css({
                            transform: "translateX(100vw)",
                            opacity: 1,
                        });
                    }, 1250);
                }
            }
        });

        //Header Scroll
        if (!$("body").hasClass("page-template-template-home")) {
            $(window).on("scroll", function () {
                if ($(window).scrollTop() >= 128) {
                    $("header .button-container").addClass("scrolling");
                } else {
                    $("header .button-container").removeClass("scrolling");
                }
            });

            $(window).on("load", function () {
                if ($(window).scrollTop() >= 128) {
                    $("header .button-container").addClass("scrolling");
                } else {
                    $("header .button-container").removeClass("scrolling");
                }
            });
        }

        //Red Underline
        if ($(".red-underline").length > 0) {
            $(".red-underline").each(function () {
                var link = $(this);
                var underline = $(this).find(".underline")[0];
                link.hover(function () {
                    TweenMax.fromTo(underline, .5, {width: "100%"}, {
                        width: "0",
                        repeat: 1,
                        yoyo: true,
                        ease: Linear.easeNone
                    });
                }, function () {
                    TweenMax.to(underline, .5, {width: "100%"});
                })
            })
        }

        //Center Images In Container
        function centerImage(imageContainer) {
            //set size
            var th = imageContainer.height(),//box height
                tw = imageContainer.width(),//box width
                im,//image
                ih,//initial image height
                iw;//initial image width
            if (imageContainer.children('img, iframe, video, picture').length > 0) {
                im = imageContainer.children('img, iframe, video');
                ih = im.height();
                iw = im.width();
            }

            if ((th / tw) > (ih / iw)) {
                im.addClass('wh').removeClass('ww');//set height 100%
            } else {
                im.addClass('ww').removeClass('wh');//set width 100%
            }
        }

        $(window).ready(function (e) {
            if ($(".image-container").length > 0) {
                $(".image-container").each(function () {
                    var imageContainer = $(this);
                    $(window).ready(function () {
                        centerImage(imageContainer);
                    });
                });

                $(window).on("resize", function () {
                    if ($(".image-container").length > 0) {
                        $(".image-container").each(function () {
                            var imageContainer = $(this);
                            setTimeout(function () {
                                centerImage(imageContainer);
                            }, 10)
                        })
                    }
                });
            }
        });

        //lazy load images
        if ($("[data-image]").length > 0) {
            $("[data-image]").each(function () {
                var image = $(this);
                var imageWaypoint = new Waypoint({
                    element: image[0],
                    handler: function () {
                        if ($(this).attr("src") === undefined || $(this).attr("src") === "") {
                            var src = image.attr("data-image");
                            image.attr("src", src);
                            setTimeout(function () {
                                centerImage($(image.parent()));
                            }, 250);
                        }
                    },
                    offset: "200%",
                })
            })
        }

        //lazy load video
        if ($("[data-video]").length > 0) {
            $("[data-video]").each(function () {
                var video = $(this);
                var imageWaypoint = new Waypoint({
                    element: video[0],
                    handler: function () {
                        if ($(this).attr("src") === undefined || $(this).attr("src") === "") {
                            var src = video.attr("data-video");
                            video.attr("src", src);
                        }
                    },
                    offset: "200%",
                })
            })
        }

        //Selectric
        if ($("select").length > 0) {
            $("select").each(function () {
                $(this).selectric();
            })
        }

        //Form Focus
        $(".gform_wrapper input,.gform_wrapper textarea").not("input:submit").on("focus", function () {
            if ($(this).parents(".ginput_container_address").length > 0 || $(this).parents(".ginput_container_name").length > 0) {
                $(this).parent().addClass("focus");
            } else {
                $(this).parent().parent().addClass("focus");
            }
            var value = $(this).val();
            if (value !== "") {
                $(this).parent().addClass("active");
            }
        });

        $(".gform_wrapper input,.gform_wrapper textarea").not("input:submit").on("blur", function () {
            if ($(this).parents(".ginput_container_address").length > 0 || $(this).parents(".ginput_container_name").length > 0) {
                $(this).parent().removeClass("focus");
            } else {
                $(this).parent().parent().removeClass("focus");
            }
        });

        $(".gform_wrapper input,.gform_wrapper textarea").not("input:submit").on("keyup", function () {
            var value = $(this).val();
            if (value !== "") {
                $(this).parent().addClass("active");
                $(this).parent().prev().addClass("active");
            } else {
                $(this).parent().removeClass("active");
                $(this).parent().prev().removeClass("active");
            }
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
