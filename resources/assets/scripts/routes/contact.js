export default {
    init() {
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        //Map
        var map;

        function CustomMarker(latlng, map) {
            this.latlng = latlng;
            this.setMap(map);
        }


        function initMap() {
            // Locations Example
            var locations_data = [
                {
                    title: "Primary Kitchen",
                    anchor: "#location-1",
                    lat: '39.7396815',
                    lng: '-104.99173',
                }
            ];


            var icons = {
                location: {icon: '../images/pin-circle.svg'},
            };

            var zoom = 12;

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 39.7396815, lng: -104.99173},
                zoom: zoom,
                styles: [
                    {
                        "featureType": "landscape",
                        "stylers": [
                            {
                                "color": "#f5f1e9"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c2efd2"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.attraction",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c2efd2"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c2efd2"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.sports_complex",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c2efd2"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "stylers": [
                            {
                                "color": "#ece6d7"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "stylers": [
                            {
                                "color": "#ece6d7"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {
                                "color": "#b3d4f8"
                            }
                        ]
                    }
                ],
            });

            map.setOptions({
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true
            });

            //PK Location Markers
            function pk_locations(locations) {
                locations.forEach(function (location) {
                    var title = location.title;
                    var address = location.address;
                    var url = location.url;
                    var position = new google.maps.LatLng(parseFloat(location.latitude), parseFloat(location.longitude));

                    CustomMarker.prototype = new google.maps.OverlayView();

                    CustomMarker.prototype.draw = function () {
                        var marker = this.div;
                        if (!marker) {
                            marker = this.div = document.createElement('a');
                            marker.className = 'pin';
                            marker.style.position = 'absolute';
                            marker.style.cursor = 'pointer';
                            marker.style.display = 'inline-block';

                            marker.style.width = '22px';
                            marker.style.height = '63px';

                            marker.setAttribute('href', url);

                            var marker_inner = document.createElement('div');
                            marker_inner.className = 'marker-inner';
                            marker_inner.style.width = '100%';
                            marker_inner.style.height = '100%';
                            marker_inner.style.position = 'relative';
                            marker.appendChild(marker_inner);

                            var number = document.createElement('div');
                            number.className = 'number';
                            number.style.width = '22px';
                            number.style.height = '22px';
                            number.style.position = 'relative';
                            marker_inner.appendChild(number);

                            var image = document.createElement('img');
                            image.setAttribute('src', icons['location'].icon);
                            image.setAttribute('alt', title + ': ' + address);
                            image.style.width = '100%';
                            image.style.height = '100%';
                            image.style.position = 'absolute';
                            image.style.top = '50%';
                            image.style.left = '50%';
                            image.style.transform = 'translate(-50%, -50%)';
                            image.style.cursor = 'pointer';
                            number.appendChild(image);

                            var pin_end = document.createElement('span');
                            pin_end.style.width = '2px';
                            pin_end.style.height = '41px';
                            pin_end.style.background = '#000000';
                            pin_end.style.display = 'inline-block';
                            pin_end.style.position = 'absolute';
                            pin_end.style.top = '22px';
                            pin_end.style.left = '50%';
                            pin_end.style.transform = 'translateX(-50%)';
                            marker_inner.append(pin_end);

                            google.maps.event.addDomListener(marker, "mouseenter", function (event) {
                                $(this).css("border", "1px solid #000000");
                            });

                            google.maps.event.addDomListener(marker, "mouseleave", function (event) {
                                $(this).css("border", "none");
                            });

                            google.maps.event.addDomListener(marker, "focus", function (event) {
                                $(this).css("border", "1px solid #000000");
                            });

                            google.maps.event.addDomListener(marker, "blur", function (event) {
                                $(this).css("border", "none");
                            });

                            var panes = this.getPanes();
                            panes.overlayImage.appendChild(marker);

                            var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

                            if (point) {
                                marker.style.left = (point.x - 15) + 'px';
                                marker.style.top = (point.y - 40) + 'px';
                            }
                        }
                    };

                    CustomMarker.prototype.remove = function () {
                        if (this.div) {
                            this.div.parentNode.removeChild(this.div);
                            this.div = null;
                        }
                    };

                    CustomMarker.prototype.getPosition = function () {
                        return this.latlng;
                    };

                    var overlay = new CustomMarker(position, map);
                })
            }

            if (typeof locations_data !== 'undefined') {
                pk_locations(locations_data);
            }
        }

        initMap();

        if (!inIframe()) {

        }
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
