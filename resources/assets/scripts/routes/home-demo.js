export default {
    init() {
        console.log("this is a test");
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        function centerImage(imageContainer) {
            //set size
            var th = imageContainer.height(),//box height
                tw = imageContainer.width(),//box width
                im,//image
                ih,//initial image height
                iw;//initial image width
            if (imageContainer.children('img').length > 0) {
                im = imageContainer.children('img');
                ih = im.height();
                iw = im.width();
            } else if (imageContainer.children('picture').length > 0) {
                im = imageContainer.children('img');
                ih = im.height();
                iw = im.width();
            }

            if ((th / tw) > (ih / iw)) {
                im.addClass('wh').removeClass('ww');//set height 100%
            } else {
                im.addClass('ww').removeClass('wh');//set width 100%
            }

            //set offset
            var nh = im.height(),//new image height
                nw = im.width(),//new image width
                hd = (nh - th) / 2,//half dif img/box height
                wd = (nw - tw) / 2;//half dif img/box width
            if (hd < 1) {
                hd = 0;
            }
            if (wd < 1) {
                wd = 0;
            }

            im.css({marginLeft: '-' + wd + 'px', marginTop: '-' + hd + 'px'});//offset left
        }

        $("#product-image-styles").on("submit", function (e) {
            e.preventDefault();
            var data = $(this).serializeArray();
            var styles = [];
            for (var i = 0; i < data.length; i++) {
                styles[data[i].name] = data[i].value;
            }
            if (styles !== undefined) {
                if (styles['product-image-width'] !== "") {
                    var rightSectionWidth = ($("#right-section").width() / $(window).width()) * 100;
                    var imageWidth = rightSectionWidth * (parseInt(styles['product-image-width']) / 100);
                    $(".product-item .product-inner").css("width", imageWidth + "vw");

                    if (styles['product-image-aspect-ratio'] !== "") {
                        var aspectRatio = styles['product-image-aspect-ratio'].split(':');
                        var imageHeight = (imageWidth * parseInt(aspectRatio[1])) / parseInt(aspectRatio[0]);
                        $(".product-item .product-inner").css("height", imageHeight + "vw");
                    } else {
                        $(".product-item .product-inner").css("height", "100vh");
                    }
                }

                if (styles['product-image-position'] !== undefined) {
                    if (styles['product-image-position'] === "left") {
                        $(".product-item").removeClass("right");
                    } else {
                        $(".product-item").addClass("right");
                    }
                }

                if (styles['has-text']) {
                    $(".product-item .text-cnt").removeClass("hidden");
                    if (styles['product-text-position-horizontal'] !== undefined) {
                        if (styles['product-text-position-horizontal'] === "left") {
                            $(".product-item .text-cnt").removeClass("right");
                            if (styles['product-text-side-margin'] !== undefined) {
                                $(".product-item .text-cnt").css("left", parseInt(styles['product-text-side-margin']) + "%");
                            }
                        } else {
                            $(".product-item .text-cnt").addClass("right");
                            if (styles['product-text-side-margin'] !== undefined) {
                                $(".product-item .text-cnt").css("right", parseInt(styles['product-text-side-margin']) + "%");
                            }
                        }
                    }

                    if (styles['product-text-position-vertical'] !== undefined) {
                        if (styles['product-text-position-vertical'] === "top") {
                            $(".product-item .text-cnt").addClass("top");
                        } else {
                            $(".product-item .text-cnt").removeClass("top");
                        }
                    }
                } else {
                    $(".product-item .text-cnt").addClass("hidden");
                }

                if (styles['has-redbox']) {
                    $(".product-item .red-box").removeClass("hidden");
                    if (styles['red-box-width'] !== "") {
                        $(".product-item .red-box").css("width", parseInt(styles['red-box-width']) + "%");
                    }

                    if (styles['red-box-height'] !== "") {
                        $(".product-item .red-box").css("height", parseInt(styles['red-box-height']) + "%");
                    }

                    if (styles['red-box-top'] !== "") {
                        $(".product-item .red-box").css("top", parseInt(styles['red-box-top']) + "%");
                    }

                    if (styles['red-box-left'] !== "") {
                        $(".product-item .red-box").css("left", parseInt(styles['red-box-left']) + "%");
                    }
                } else {
                    $(".product-item .red-box").addClass("hidden");
                }

                centerImage($(".product-item .image-container"));
            }
        });

        $(".product-item").on("click", function () {
            if(!$(this).hasClass("editable")){
                $(".product-item").removeClass("editable");
                $(this).addClass("editable");
            }
        })
    },
};
