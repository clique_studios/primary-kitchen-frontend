export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        if (!inIframe()) {
            //Product Image Style
            function imageSize(image, aspectRatio) {
                var imageInner = image.children(".product-inner");
                var imageContainer = imageInner.children(".image-container");
                var rightSectionWidth, imageWidth, imageMarginLeft, imageHeight;

                if ($(window).width() > 1024) {
                    rightSectionWidth = (($(window).width() - $("#left-section").width()) / $(window).width()) * 100;
                    imageWidth = image[0].dataset.width;
                    imageMarginLeft = image[0].dataset.marginLeft;
                    image.css("padding", 0);
                } else if ($(window).width() > 768) {
                    rightSectionWidth = 100;
                    imageWidth = image[0].dataset.width;
                    imageMarginLeft = image[0].dataset.marginLeft;
                    image.css("padding", 0);
                } else if ($(window).width() > 581) {
                    rightSectionWidth = 100;
                    imageWidth = image[0].dataset.responsiveWidth;
                    imageMarginLeft = image[0].dataset.marginLeftResponsive;
                    image.css("padding", 0);
                } else {
                    rightSectionWidth = 100;
                    imageWidth = "100%";
                    imageMarginLeft = 0;
                    if (image[0].dataset.mobilePadding !== undefined) {
                        image.css({
                            paddingLeft: image[0].dataset.mobilePadding,
                            paddingRight: image[0].dataset.mobilePadding,
                        });
                    }
                }

                imageHeight = (rightSectionWidth * (parseInt(imageWidth) / 100) * parseInt(aspectRatio[1])) / parseInt(aspectRatio[0]);
                imageInner.css("width", imageWidth);
                imageContainer.css("height", imageHeight + "vw");
                if (imageMarginLeft !== undefined) {
                    imageInner.css("margin-left", imageMarginLeft);
                } else {
                    imageInner.css("margin-left", 0);
                }
            }

            if ($(".product-item").not(".banner").length > 0) {
                $(".product-item").not(".banner").each(function () {
                    var image = $(this);
                    var aspectRatio = $(this)[0].dataset.aspectRatio.split(':');
                    imageSize(image, aspectRatio);

                    $(window).on("resize", function () {
                        imageSize(image, aspectRatio);
                    })
                })
            }

            //Header Scroll Style
            $("#right-section").on("scroll", function () {
                var scrollTop = ($("#right-section .section-inner").offset().top - 126) * -1;
                if (scrollTop >= 1) {
                    $("header .button-container").addClass("scrolling");
                } else {
                    $("header .button-container").removeClass("scrolling");
                }
            });

            $(window).on("resize", function () {
                if ($(window).width() <= 1024) {
                    $("header").removeClass("scrolling");
                }
            });

            //Text Highlight
            if ($("#left-section .highlight").not("a").length > 0) {
                $("#left-section .highlight").not("a").each(function () {
                    var element = $(this);
                    var highlightWaypoint = new Waypoint({
                        element: element[0],
                        handler: function (direction) {
                            if (direction === "down") {
                                if (!element.hasClass("grow")) {
                                    element.addClass("grow");
                                }
                            }
                        },
                        offset: "80%",
                        context: document.getElementById('left-section'),
                    })
                });
            }

            if ($("#right-section .highlight").not("a").length > 0) {
                $("#right-section .highlight").not("a").each(function () {
                    var element = $(this);
                    var highlightWaypoint = new Waypoint({
                        element: element[0],
                        handler: function (direction) {
                            if (direction === "down") {
                                if (!element.hasClass("grow")) {
                                    element.addClass("grow");
                                }
                            }
                        },
                        offset: ($(window).height() - 15) + "px",
                        context: document.getElementById('right-section'),
                    })
                });
            }

            if ($('.highlight').not("a").length > 0) {
                $('.highlight').not("a").each(function () {
                    var element = $(this);
                    var highlightWaypoint = new Waypoint({
                        element: element[0],
                        handler: function (direction) {
                            if ($(window).width() <= 1024) {
                                if (direction === "down") {
                                    if (!element.hasClass("grow")) {
                                        element.addClass("grow");
                                    }
                                }
                            }
                        },
                        offset: "90%",
                    })
                })
            }
        }
    },
};
