export default {
    init() {

    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        if (!inIframe()) {
            function centerImage(imageContainer) {
                //set size
                var th = imageContainer.height(),//box height
                    tw = imageContainer.width(),//box width
                    im,//image
                    ih,//initial image height
                    iw;//initial image width
                if (imageContainer.children('img').length > 0) {
                    im = imageContainer.children('img');
                    ih = im.height();
                    iw = im.width();
                } else if (imageContainer.children('picture').length > 0) {
                    im = imageContainer.children('img');
                    ih = im.height();
                    iw = im.width();
                }

                if ((th / tw) > (ih / iw)) {
                    im.addClass('wh').removeClass('ww');//set height 100%
                } else {
                    im.addClass('ww').removeClass('wh');//set width 100%
                }
            }

            function isEmpty(myObject) {
                for (var key in myObject) {
                    if (myObject.hasOwnProperty(key)) {
                        return false;
                    }
                }

                return true;
            }

            //Product Flow
            var selected_options = {'primary': {}, 'secondary': {}}, next_disabled = true, clickedOption;

            //Start Product Flow
            $(".start-flow").bind("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                var next_section = $($(".product-flow-section")[1]);
                $("#products-list, footer").addClass("hidden");
                $("#products-list").attr("aria-hidden", true);
                $(".product-flow-section.landing-flow").removeClass("active").addClass("prev");
                setTimeout(function () {
                    $(".product-flow-section.landing-flow .flow-inner").addClass("hidden");
                }, 500);
                $($(".product-flow-section")[0]).attr("aria-hidden", false);
                $($(".product-flow-section")[1]).attr("aria-hidden", false);
                $(this).attr("aria-hidden", false);
                $($(".product-flow-section")[1]).find(".flow-inner").removeClass("hidden");
                next_section.addClass("active");
                if ($(next_section.find(".col-xs-6 img")[0]).attr("src") === "") {
                    next_section.find(".col-xs-6 img").attr("src", $(next_section.find(".col-xs-6 img")[0]).attr("data-init-image"));
                    centerImage($($(next_section.find(".col-xs-6 .image-container")[0])));
                }
                $("header").addClass("in-product-flow");
                $('html,body').animate({scrollTop: $("#product-flow").offset().top}, 'slow');
            });

            //Exit Product Flow
            $("#product-flow .exit").bind("click", function (e) {
                e.preventDefault();
                $("#products-list, footer").removeClass("hidden");
                $("#products-list").attr("aria-hidden", false);
                $(".product-flow-section").addClass("reset");
                setTimeout(function () {
                    $(".product-flow-section").removeClass("reset");
                }, 100);
                $(".product-flow-section").removeClass("active prev");
                $(".product-flow-section .flow-inner").addClass("hidden");
                $(".product-flow-section.landing-flow").removeClass("prev").addClass("active");
                $(".product-flow-section.landing-flow").attr("aria-hidden", false);
                $(".product-flow-section.landing-flow .flow-inner").removeClass("hidden");

            });

            $("#product-flow .option-group label").bind("click", function () {
                var label = $(this), section = $(this).parents(".product-flow-section"), parent = $(this).parent();
                section.find(".description").html(parent.attr("data-description"));

                //Update Section Image
                if (parent.attr("data-image-src") !== undefined) {
                    $(section.find(".image-container img")[0]).attr("src", parent.attr("data-image-src"));
                    centerImage($(section.find(".image-container")[0]));
                }

                //Update focus state
                if (!label.prev(".radiobox").hasClass("focus")) {
                    $("#product-flow .radiobox").removeClass("focus");
                    label.prev(".radiobox").addClass("focus");
                }

                //Update Select Button Text
                var list_count = section.find(".option-group").length,
                    select_count = section.find(".select-button-container .select-options").length / 2, select_button,
                    list = $(this).parents(".option-group");
                section.find(".select-button-container").each(function () {
                    if (list_count > select_count) {
                        select_button = $($(this).find(".select-options")[0]);
                    } else if (select_count > list_count) {
                        if ($(this).attr("data-select-index") !== undefined) {
                            select_button = $($(this).find(".select-options")[$(this).attr("data-select-index")]);
                        }
                    }

                    if (select_button !== undefined && select_button !== null) {
                        var select_button_text = select_button.children("span").text();
                        if (label.prev(".radiobox").hasClass("checked")) {
                            if (select_button_text.toLowerCase() === "select") {
                                select_button_text = select_button_text.replace(/Select/gi, "Remove Selection");
                            }
                        } else {
                            select_button_text = select_button_text.replace(/Remove Selection/gi, "Select");
                        }

                        select_button.children("span").text(select_button_text);
                    }
                });

                clickedOption = label.prev(".radiobox");
            });

            $("#product-flow .select-options").on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                var section = $(this).parents(".product-flow-section");
                if (clickedOption !== undefined) {
                    var list_count = section.find(".option-group").length,
                        select_count = $(this).parent().children().length,
                        list_index = clickedOption.parents(".option-group").index(), select_index = $(this).index(),
                        key;
                    if (list_count > select_count) {
                        key = list_index === 0 ? 'primary' : 'secondary'
                    } else if (list_count < select_count) {
                        key = select_index === 0 ? 'primary' : 'secondary';
                    }
                    if (clickedOption.hasClass("checked")) {

                        //Remove Selected Option
                        clickedOption.removeClass("checked");
                        clickedOption.next("label").removeAttr("data-select-index");
                        $(section.find(".option-group")[list_index]).find("input").each(function () {
                            var name = $(this).attr("name");
                            if (name === clickedOption.children("input").attr("name")) {
                                selected_options[key][name] = [];
                                if (key === "primary") {
                                    next_disabled = true;
                                    section.find("button.next").attr("disabled", "disabled");
                                }
                            }
                        });
                        section.find(".select-button-container").each(function () {
                            var select_button = $($(this).find(".select-options")[select_index]);
                            select_button.children("span").text(select_button.children("span").text().replace(/Remove Selection/gi, "Select"));
                        });
                    } else {

                        //Select Option
                        $(section.find(".option-group")[list_index]).find("input").each(function () {
                            var name = $(this).attr("name");
                            var parent = $(this).parent();
                            var update = true;
                            if(clickedOption.next("label").attr("data-select-index") !== undefined && clickedOption.next("label").attr("data-select-index") !== select_index) {
                                update = false;
                            }
                            if ($(this).prop("checked")) {
                                if(update) {
                                    if (selected_options[key][name] === undefined) {
                                        selected_options[key][name] = [];
                                    }
                                    $(this).parent().addClass("checked");
                                    $(this).parent().next("label").attr("data-select-index", select_index);
                                    if (selected_options[key][name].length > 0) {
                                        selected_options[key][name] = [];
                                    }
                                    selected_options[key][name].push($(this).attr("value"), parent.parent().attr("data-image-src"));
                                    if (key === "primary") {
                                        next_disabled = false;
                                    }
                                }
                            } else {
                                if(update){
                                    if (key === "secondary") {
                                        if (isEmpty(selected_options["primary"][name])) {
                                            $(this).parent().removeClass("checked");
                                        }
                                    } else if (key === "primary") {
                                        if (isEmpty(selected_options["secondary"][name])) {
                                            $(this).parent().removeClass("checked");
                                        }
                                    }
                                }
                            }
                        });

                        section.find(".select-button-container").each(function () {
                            var select_button = $($(this).find(".select-options")[select_index]);
                            select_button.children("span").text(select_button.children("span").text().replace(/Select/gi, "Remove Selection"));
                        });
                    }
                }

                if (!next_disabled) {
                    section.find("button.next").removeAttr("disabled");
                }
            });

            $("#product-flow .prev").on("click", function () {
                var section = $(this).parents(".product-flow-section");
                var prev_section = section.prev();
                section.removeClass("active");
                prev_section.addClass("active");
                prev_section.find(".flow-inner").removeClass("hidden");
                setTimeout(function () {
                    section.find(".flow-inner").addClass("hidden");
                }, 400);

                setTimeout(function () {
                    prev_section.removeClass("prev");
                }, 500);
                $(this).attr("aria-expanded", true);
                prev_section.attr("aria-hidden", false);
                section.attr("aria-hidden", true);
                $(prev_section.find("input")[0]).focus();
            });

            $("#product-flow .next").on("click", function () {
                var section = $(this).parents(".product-flow-section");
                var next_section = section.next();
                section.addClass("prev");
                section.removeClass("active");
                setTimeout(function () {
                    section.find(".flow-inner").addClass("hidden");
                }, 400);
                next_section.addClass("active");
                next_section.find(".flow-inner").removeClass("hidden");
                if ($(next_section.find(".col-xs-6 img")[0]).attr("src") === "") {
                    next_section.find(".col-xs-6 img").attr("src", $(next_section.find(".col-xs-6 img")[0]).attr("data-init-image"));
                    centerImage($($(next_section.find(".col-xs-6 .image-container")[0])));
                }
                if (next_section.find(".prev-options").length > 0) {
                    var prev_options_text = "";
                    for (var key in selected_options) {
                        for (var name in selected_options[key]) {
                            if (selected_options[key][name][0] !== undefined) {
                                prev_options_text += "<p>" + selected_options[key][name][0] + "</p>";
                            }
                        }
                    }

                    $(next_section.find(".prev-options")[0]).empty().append(prev_options_text);
                }

                if (next_section.find(".select-button-container .select-options").length > 0) {
                    next_section.find(".select-button-container").each(function () {
                        if (Object.keys(selected_options).length <= 1) {
                            $($(this).find(".select-options")[1]).addClass("hidden");
                        } else {
                            $($(this).find(".select-options")[1]).removeClass("hidden");
                        }
                    });
                }

                if (next_section.find(".results").length > 0) {
                    var results = "";
                    for (var key in selected_options) {
                        for (var name in selected_options[key]) {
                            var label = name.replace(/-/g, ' ');
                            label = key === "secondary" ? "Secondary " + label : label.charAt(0).toUpperCase() + label.slice(1);
                            if (!isEmpty(selected_options[key][name])) {
                                var option_name = selected_options[key][name][0].replace(/-/g, ' ');
                                option_name = option_name.charAt(0).toUpperCase() + option_name.slice(1);
                                results += "<div class='option'>" + "<h4>" + label + "</h4>" + "<button" +
                                    " class='final-option' data-image-src='" + selected_options[key][name][1] + "'>" + option_name + "</button>" + "</div>";
                            }
                        }
                    }

                    $(next_section.find(".results")[0]).empty().append(results);
                }
                next_disabled = true;
                $(this).attr("aria-expanded", true);
                next_section.attr("aria-hidden", false);
                section.attr("aria-hidden", true);
                $(next_section.find("input")[0]).focus();
            });

            $(document).on("click", ".final-option", function (e) {
                e.preventDefault();
                var section = $(this).parents(".product-flow-section");
                $(section.find(".image-container img")[0]).attr("src", $(this).attr("data-image-src"));
                centerImage($(section.find(".image-container")[0]));
            });

            //Mobile Modal
            var product_info_button;
            $(".option-group .button-container a").on("click", function (e) {
                e.preventDefault();
                product_info_button = $(this);
                var title = $(this).parent().prev().text();
                var description = $(this).parent().parent().attr("data-description");
                var img = $(this).parent().parent().attr("data-image-src");
                var alt = $(this).parent().parent().attr("data-image-alt");
                $("#js-modal").css("display", "block");
                $("#js-modal").find(".product-name").html(title);
                if ($(this).hasClass("details-modal")) {
                    $("#js-modal").find(".description").removeClass("hidden").html(description);
                    $("#js-modal").find(".image-container").addClass("hidden");
                } else {
                    $("#js-modal").find(".image-container").removeClass("hidden");
                    $("#js-modal").find(".description").addClass("hidden");
                    setTimeout(function () {
                        $("#js-modal").find(".image-container img").attr({
                            "src": img,
                            "alt": alt
                        });
                        centerImage($($("#js-modal").find(".image-container img")[0]));
                    }, 100);
                }
                $("#modal-close").focus();
            });

            $("#modal-close").on("click", function () {
                $("#js-modal").css("display", "none");
                $("#js-modal").find(".image-container img").attr("src", "");
                product_info_button.focus();
            });

            // Products List
            function scrollToAnchor(aid) {
                var aTag = $(aid);
                $('html,body').animate({scrollTop: aTag.offset().top}, 'slow');
            }

            $(".to-list").on("click", function (e) {
                e.preventDefault();
                scrollToAnchor("#products-list");
            });

            $(".series-container button").on("click", function () {
                var listParent = $(this).parent().parent();
                listParent.find("button").removeClass("active").attr("aria-expanded", false);
                $(this).addClass("active").attr("aria-expanded", true);

                listParent.next().find("li").removeClass("active");
                var link = $(this).attr("aria-controls");
                $("#" + link).addClass("active");

                var img = $(listParent.parent().parent().find(".image-container")[0]);
                img.children("img").attr({
                    'src': $(this).attr("data-img-src"),
                    'alt': $(this).attr("data-img-alt"),
                });
                centerImage(img);
            });

            $(".series-link-container a").on("click", function (e) {
                e.preventDefault();
                scrollToAnchor($(this).attr("href"));
            })
        }
    },
};