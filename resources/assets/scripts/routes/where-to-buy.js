export default {
    init() {
        function inIframe() {
            try {
                return window.self !== window.top;
            } catch (e) {
                return true;
            }
        }

        function scrollToAnchor(aid) {
            var aTag = $(aid);
            var offset = aTag.offset().top;
            if ($(window).width() > 581) {
                $('#locations .col-xs-5 .scrollable').animate({scrollTop: offset}, 'slow');
            } else {
                $('html,body').animate({scrollTop: offset}, 'slow');
            }
        }

        //Map
        var map;
        var marker_index = 1;
        var no_results = true;

        function CustomMarker(latlng, map) {
            this.latlng = latlng;
            this.setMap(map);
        }

        function pk_locations(locations_data, icons, radius_limit) {
            locations_data.forEach(function (location) {
                var title = location.title;
                var address = "";
                location.address.forEach(function (line) {
                    address += line + " ";
                });
                var anchor = location.anchor;

                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({address: address},
                    function (results_array, status) {
                        if (status === "OK") {
                            var num = marker_index;

                            CustomMarker.prototype = new google.maps.OverlayView();
                            CustomMarker.prototype.draw = function () {
                                var marker = this.div;
                                if (!marker) {
                                    marker = this.div = document.createElement('a');
                                    marker.className = 'pin';
                                    marker.style.position = 'absolute';
                                    marker.style.cursor = 'pointer';
                                    marker.style.display = 'inline-block';

                                    marker.style.width = '22px';
                                    marker.style.height = '63px';

                                    marker.setAttribute('href', anchor);

                                    var marker_inner = document.createElement('div');
                                    marker_inner.className = 'marker-inner';
                                    marker_inner.style.width = '100%';
                                    marker_inner.style.height = '100%';
                                    marker_inner.style.position = 'relative';
                                    marker.appendChild(marker_inner);

                                    var number = document.createElement('div');
                                    number.className = 'number';
                                    number.style.width = '22px';
                                    number.style.height = '22px';
                                    number.style.position = 'relative';
                                    marker_inner.appendChild(number);

                                    var number_text = document.createElement('p');
                                    number_text.innerHTML = num;
                                    number_text.style.color = '#FFFFFF';
                                    number_text.style.fontSize = '12px';
                                    number_text.style.position = 'absolute';
                                    number_text.style.top = '50%';
                                    number_text.style.left = '50%';
                                    number_text.style.transform = 'translate(-50%, -50%)';
                                    number_text.style.zIndex = 1;
                                    number.append(number_text);

                                    var image = document.createElement('img');
                                    image.setAttribute('src', icons['location'].icon);
                                    image.setAttribute('alt', title + ': ' + address);
                                    image.style.width = '100%';
                                    image.style.height = '100%';
                                    image.style.position = 'absolute';
                                    image.style.top = '50%';
                                    image.style.left = '50%';
                                    image.style.transform = 'translate(-50%, -50%)';
                                    image.style.cursor = 'pointer';
                                    number.appendChild(image);

                                    var pin_end = document.createElement('span');
                                    pin_end.style.width = '2px';
                                    pin_end.style.height = '41px';
                                    pin_end.style.background = '#000000';
                                    pin_end.style.display = 'inline-block';
                                    pin_end.style.position = 'absolute';
                                    pin_end.style.top = '22px';
                                    pin_end.style.left = '50%';
                                    pin_end.style.transform = 'translateX(-50%)';
                                    marker_inner.append(pin_end);

                                    google.maps.event.addDomListener(marker, "mouseenter", function (event) {
                                        $(this).css("border", "1px solid #000000");
                                    });

                                    google.maps.event.addDomListener(marker, "mouseleave", function (event) {
                                        $(this).css("border", "none");
                                    });

                                    google.maps.event.addDomListener(marker, "blur", function (event) {
                                        $(this).css("border", "none");
                                    });

                                    google.maps.event.addDomListener(marker, "click", function (e) {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        var screenWidth = $(window).width();
                                        if (screenWidth > 581) {
                                            scrollToAnchor(anchor);
                                        } else {
                                            scrollToAnchor(anchor + "-mobile");
                                        }
                                    });

                                    google.maps.event.addDomListener(marker, "focus", function (event) {
                                        $(this).css("border", "1px solid #000000");
                                    });

                                    var panes = this.getPanes();
                                    panes.overlayImage.appendChild(marker);

                                    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

                                    if (point) {
                                        marker.style.left = (point.x - 15) + 'px';
                                        marker.style.top = (point.y - 40) + 'px';
                                    }
                                }
                            };

                            CustomMarker.prototype.remove = function () {
                                if (this.div) {
                                    this.div.parentNode.removeChild(this.div);
                                    this.div = null;
                                }
                            };

                            CustomMarker.prototype.getPosition = function () {
                                return this.latlng;
                            };

                            var lat = results_array[0].geometry.location.lat();
                            var lng = results_array[0].geometry.location.lng();

                            if (radius_limit !== undefined) {
                                if (google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), radius_limit.getCenter()) <= radius_limit.getRadius()) {
                                    var overlay = new CustomMarker(new google.maps.LatLng(lat, lng), map);
                                    no_results = false;
                                }
                            } else {
                                var overlay = new CustomMarker(new google.maps.LatLng(lat, lng), map);
                            }

                            if (no_results) {
                                $(".no-results").removeClass("hidden");
                            }
                        }
                        marker_index++;
                    });
            });
        }

        //Get Zip and Radius
        var url_string = window.location.href;
        var url = new URL(url_string);
        var zip_code = url.searchParams.get("zip-code");
        var radius = url.searchParams.get("radius");
        if (zip_code !== null) {
            var zip_text = "<p>" + zip_code + "</p>";
            $(".zip-radius").append(zip_text);
        }
        if (radius !== null) {
            var radius_text = "<p>" + radius + " miles</p>";
            $(".zip-radius").append(radius_text);
        }

        function initMap() {
            if (zip_code !== null) {
                $("#locations .results").removeClass("hidden");
            }

            if (radius !== null && radius !== "") {
                radius = radius * 1609.344;
            } else {
                radius = 50 * 1609.344;
            }

            // Locations Example
            var locations_data = [
                {
                    title: "location 1",
                    anchor: "#location-1",
                    address: ['2300 Steele St', 'Denver, CO 80205'],
                },
                {
                    title: "location 2",
                    anchor: "#location-2",
                    address: ['1001 16th St Mall', 'Denver, CO 80265'],
                },
                {
                    title: "location 3",
                    anchor: "#location-3",
                    address: ['2001 Blake St', 'Denver, CO 80265'],
                },
                {
                    title: "location 4",
                    anchor: "#location-4",
                    address: ['100 W 14th Ave Pkwy', 'Denver, CO 80204'],
                },
                {
                    title: "location 5",
                    anchor: "#location-5",
                    address: ['1727 Tremont Pl', 'Denver, CO 80202'],
                },
            ];


            var icons = {
                location: {icon: '../images/pin-circle.svg'},
            };

            var zoom = 12.5;

            //Geolocation

            //Zip Code
            if (zip_code !== null) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({address: zip_code},
                    function (results_array, status) {
                        if (status === "OK") {
                            var lat = results_array[0].geometry.location.lat();
                            var lng = results_array[0].geometry.location.lng();
                            map.setCenter(new google.maps.LatLng(lat, lng));

                            var radius_limit = new google.maps.Circle({
                                radius: radius,
                                map: map,
                                center: {lat, lng},
                                strokeOpacity: 0,
                                fillOpacity: 0,
                            })

                            if (!inIframe()) {
                                //PK Location Markers
                                if (typeof locations_data !== 'undefined') {
                                    pk_locations(locations_data, icons, radius_limit);
                                }
                            }
                        }
                    });
            }

            //Locations

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 39.7396815, lng: -104.99173},
                zoom: zoom,
                styles: [{"featureType": "landscape", "stylers": [{"color": "#f5f1e9"}]}, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{"color": "#c2efd2"}]
                }, {
                    "featureType": "poi.attraction",
                    "elementType": "geometry",
                    "stylers": [{"color": "#c2efd2"}]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{"color": "#c2efd2"}]
                }, {
                    "featureType": "poi.sports_complex",
                    "elementType": "geometry",
                    "stylers": [{"color": "#c2efd2"}]
                }, {"featureType": "road", "stylers": [{"color": "#ece6d7"}]}, {
                    "featureType": "transit",
                    "stylers": [{"color": "#ece6d7"}]
                }, {"featureType": "water", "stylers": [{"color": "#b3d4f8"}]}],
            });

            if (!inIframe()) {
                map.setOptions({
                    draggable: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: true
                });
            }
        }

        initMap();

        if (!inIframe()) {
            //Results Scrolling
            var formHeight = $("#locations form").height() + parseFloat($("#locations form").css("padding-bottom"));
            $("#locations .col-xs-5 .scrollable").on("scroll", function () {
                var form = $("#locations .col-xs-5 form");
                var resultsWaypoint = new Waypoint({
                    element: $("#locations .col-xs-5 .sticky-trigger")[0],
                    handler: function (direction) {
                        if (direction === "down") {
                            form.addClass("sticky");
                            $(".form-placeholder").height(formHeight);

                        } else {
                            form.removeClass("sticky");
                            $(".form-placeholder").removeAttr("style");
                        }
                    },
                    offset: 0,
                    context: $("#locations .col-xs-5")[0],
                })
            });
        }
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
